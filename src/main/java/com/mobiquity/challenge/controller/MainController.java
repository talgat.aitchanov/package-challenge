package com.mobiquity.challenge.controller;

import com.mobiquity.challenge.service.PackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/main")
public class MainController {

  private PackingService packingService;

  @Autowired
  MainController(PackingService packingService) {
    this.packingService = packingService;
  }

  @GetMapping("/pack")
  public List<String> pack() {
    return packingService.pack();
  }

}
