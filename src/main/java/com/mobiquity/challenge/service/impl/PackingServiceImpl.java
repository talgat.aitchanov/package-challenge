package com.mobiquity.challenge.service.impl;

import com.mobiquity.challenge.configuration.FileConfiguration;
import com.mobiquity.challenge.model.Item;
import com.mobiquity.challenge.parser.FileParser;
import com.mobiquity.challenge.service.PackingService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.mobiquity.challenge.util.Constant.ITEM_DELIMITER;
import static com.mobiquity.challenge.util.Constant.NO_ITEM_REPRESENTATION;

@Slf4j
@Service
@AllArgsConstructor
public class PackingServiceImpl implements PackingService {

  private static final double MAX_PACKAGE_WEIGH = 100d;
  private static final double MAX_ITEM_WEIGHT = 100d;
  private static final double MAX_ITEM_PRICE = 100d;
  private static final double MAX_ITEM_COUNT = 15;
  private static final Consumer<Item> calculatePriceToWeight = item -> item.setPriceToWeight(item.getPrice() / item.getWeight());

  private final FileConfiguration fileConfiguration;

  @Override
  public List<String> pack() {

    FileParser fileParser = new FileParser();
    File file = new File(fileConfiguration.getFilePath());
    Map<String, List<Item>> maxWeightToItems = fileParser.parse(file);

    Map<String, List<Item>> rearrangedMaxWeightToItems = rearrangeItems(maxWeightToItems);

    List<String> returnResult = new LinkedList<>();
    for (List<Item> items : rearrangedMaxWeightToItems.values()) {
      returnResult.add(itemsToStringList(items));
    }

    return returnResult;
//    return Collections.unmodifiableList(Collections.emptyList());
  }

  private Map<String, List<Item>> rearrangeItems(Map<String, List<Item>> maxWeightToItems) {
    for (Entry<String, List<Item>> entry : maxWeightToItems.entrySet()) {
      String currentKey = entry.getKey();
      Double currentPackageMaxWeight = Double.valueOf(currentKey);
      List<Item> itemList = entry.getValue();
      if (itemList.size() > MAX_ITEM_COUNT) {
        log.info("Skipped. Items quantity is greater than {}", MAX_ITEM_COUNT);
        continue;
      }

      calculatePriceToWeigth(itemList);
      sortByPriceToWeight(itemList);
      List<Item> sortedItemList = itemList;
      List<Item> packedItems = new LinkedList<>();

      double currentPackageWeight = 0d;
      for (Item item : sortedItemList) {

        if (currentPackageWeight >= MAX_PACKAGE_WEIGH) {
          break;
        }

        if (!doesItemSatisfyConditions(item)) {
          break;
        }

        if (currentPackageWeight + item.getWeight() <= currentPackageMaxWeight) {
          packedItems.add(item);
          currentPackageWeight += item.getWeight();
        }

      }

      maxWeightToItems.put(currentKey, packedItems);
    }
    return maxWeightToItems;
  }

  private String itemsToStringList(List<Item> items) {
    if (CollectionUtils.isEmpty(items)) {
      return NO_ITEM_REPRESENTATION;
    }
    return items.stream()
        .map(Item::getIndexStr)
        .collect(Collectors.joining(ITEM_DELIMITER));
  }

  private void calculatePriceToWeigth(List<Item> itemList) {
    itemList.forEach(calculatePriceToWeight);
  }

  private void sortByPriceToWeight(List<Item> itemList) {
    itemList.sort(Comparator.comparing(Item::getPriceToWeight));
    Collections.reverse(itemList);
  }

  private boolean doesItemSatisfyConditions(Item item) {
    return item.getWeight() <= MAX_ITEM_WEIGHT
        && item.getPrice() <= MAX_ITEM_PRICE;
  }

}
