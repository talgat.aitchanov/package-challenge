package com.mobiquity.challenge.service;

import java.util.List;

public interface PackingService {

  List<String> pack();

}
