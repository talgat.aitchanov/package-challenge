package com.mobiquity.challenge.parser;

import com.mobiquity.challenge.exception.APIException;
import com.mobiquity.challenge.model.Item;
import com.mobiquity.challenge.util.FileUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.mobiquity.challenge.util.Constant.EMPTY_STRING;
import static com.mobiquity.challenge.util.Constant.ITEM_DELIMITER;

@Slf4j
public class FileParser {

  private static final String AVAILABLE_EXTENSION = ".txt";

  private static final String LINE_PATTERN = "^[1-9]{1,}\\s:(\\s?\\([0-9]{1,},[0-9]{1,}\\.[0-9]{1,},€[0-9]{1,}\\)){1,}$";
  private static final String ALTERNATIVE_LINE_PATTERN = "^[1-9]+ :(\\s?\\([0-9]+,[0-9]+\\.[0-9]+,€[0-9]+\\))+$";
  private static final String TOKENIZER_PATTERN = "(\\s:\\s|\\s)";
  private static final int KEY_INDEX = 0;
  private static final int ITEM_INDEX_INDEX = 0;
  private static final int ITEM_WEIGHT_INDEX = 1;
  private static final int ITEM_COST_INDEX = 2;

  public Map<String, List<Item>> parse(File file) {
    checkIsItFile(file);
    checkExtension(file);
    List<String> lines = getLines(file);
    log.info("File contains: {} lines", lines.size());
    lines = trimLines(lines);
    log.info("Going to filter according to pattern");
    lines = getValidLines(lines);
    log.info("After filtering valid lines: {}", lines.size());
    Map<String, List<Item>> maxWeightToItems = getMaxWeightToItems(lines);
    return maxWeightToItems;
  }

  private void checkIsItFile(File file) {
    if (!FileUtils.isFile(file)) {
      throw new APIException(String.format("Provided filepath: %s is not a file", FileUtils.getAbsolutePath(file)));
    }
  }

  private void checkExtension(File file) {
    if (!file.getName().endsWith(AVAILABLE_EXTENSION)) {
      throw new APIException(String.format("Provided file: %s has wrong extension. Use \".txt\"", FileUtils.getAbsolutePath(file)));
    }
  }

  private List<String> getLines(File file) {
    try {
      return Files.lines(file.toPath()).collect(Collectors.toList());
    } catch (IOException e) {
      throw new APIException(String.format("Couldn't getLines in %s file", FileUtils.getAbsolutePath(file)), e);
    }
  }

  private List<String> trimLines(List<String> lines) {
    return lines.stream().map(String::trim).collect(Collectors.toList());
  }

  private List<String> getValidLines(List<String> lines) {
    return lines.stream().filter(this::filterAccordingToPattern).collect(Collectors.toList());
  }

  private boolean filterAccordingToPattern(String line) {
    Pattern pattern = Pattern.compile(ALTERNATIVE_LINE_PATTERN);
    Matcher matcher = pattern.matcher(line);
    return matcher.find();
  }

  private Map<String, List<Item>> getMaxWeightToItems(List<String> lines) {
    Map<String, List<Item>> maxWeightToItems = new LinkedHashMap<>();
    for (String line : lines) {
      String[] tokenizedLine = tokenizeLine(line);
      String key = tokenizedLine[KEY_INDEX];
      putKey(maxWeightToItems, key);
      List<Item> itemList = getItemsFromTokenizedLine(tokenizedLine);
      putValues(maxWeightToItems, key, itemList);
    }
    return maxWeightToItems;
  }

  private String[] tokenizeLine(String line) {
    String[] preResult = line.split(TOKENIZER_PATTERN);
    return preResult;
  }

  private void putKey(Map<String, List<Item>> maxWeightToItems, String key) {
    if (!maxWeightToItems.containsKey(key)) {
      maxWeightToItems.put(key, null);
    }
  }

  private List<Item> getItemsFromTokenizedLine(String[] tokenizedLine) {
    List<Item> itemList = new LinkedList<>();
    for (int index = 0; index < tokenizedLine.length; index++) {
      if (index != KEY_INDEX) {
        String element = tokenizedLine[index];
        Item item = getItemFromElement(element);
        itemList.add(item);
      }
    }
    return itemList;
  }

  private void putValues(Map<String, List<Item>> maxWeightToItems, String key, List<Item> itemList) {
    maxWeightToItems.put(key, itemList);
  }

  private Item getItemFromElement(String element) {
    String[] splicedElement = element
        .replace("(", EMPTY_STRING)
        .replace("€", EMPTY_STRING)
        .replace(")", EMPTY_STRING)
        .split(ITEM_DELIMITER);

    return Item.builder()
        .index(
            Long.valueOf(splicedElement[ITEM_INDEX_INDEX])
        )
        .weight(
            Double.valueOf(splicedElement[ITEM_WEIGHT_INDEX])
        )
        .price(
            Double.valueOf(splicedElement[ITEM_COST_INDEX])
        )
        .indexStr(
            splicedElement[ITEM_INDEX_INDEX]
        )
        .build();
  }

}
