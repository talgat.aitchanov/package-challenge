package com.mobiquity.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PackageChallengeApplication {

  public static void main(String[] args) {
    SpringApplication.run(PackageChallengeApplication.class, args);
  }

}
