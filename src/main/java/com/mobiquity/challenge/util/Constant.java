package com.mobiquity.challenge.util;

public class Constant {
  public static final String NO_ITEM_REPRESENTATION = "-";
  public static final String ITEM_DELIMITER = ",";
  public static final String EMPTY_STRING = "";
}
