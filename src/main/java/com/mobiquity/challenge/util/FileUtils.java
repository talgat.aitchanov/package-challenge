package com.mobiquity.challenge.util;

import java.io.File;

public class FileUtils {

  public static boolean isFile(File file) {
    return file.isFile();
  }

  public static String getAbsolutePath(File file) {
    return file.getAbsolutePath();
  }

}
