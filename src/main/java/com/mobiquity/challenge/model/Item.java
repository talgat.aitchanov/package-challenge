package com.mobiquity.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Item {

  private Long index;

  private Double price;

  private Double weight;

  private Double priceToWeight;

  private String indexStr;

}
