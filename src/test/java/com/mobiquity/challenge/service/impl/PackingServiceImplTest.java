package com.mobiquity.challenge.service.impl;

import com.mobiquity.challenge.configuration.FileConfiguration;
import com.mobiquity.challenge.service.PackingService;
import junit.framework.TestCase;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Objects;

@Slf4j
public class PackingServiceImplTest {

  private PackingService packingService;

  @Before
  public void start() {
    ClassLoader classLoader = getClass().getClassLoader();
    String filePath = Objects.requireNonNull(classLoader.getResource("test.txt")).getPath();
    FileConfiguration fileConfiguration = new FileConfiguration();
    fileConfiguration.setFilePath(filePath);
    packingService = new PackingServiceImpl(fileConfiguration);
  }

  @Test
  public void pack() {
    List<String> packedResult = packingService.pack();
    for (String s : packedResult) {
      log.info(s);
    }
    TestCase.assertNotNull("Not null", packedResult);
  }
}