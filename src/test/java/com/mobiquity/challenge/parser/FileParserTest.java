package com.mobiquity.challenge.parser;

import com.mobiquity.challenge.model.Item;
import junit.framework.TestCase;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
public class FileParserTest {

  private File file;

  private FileParser fileParser;

  @Before
  public void start() {
    String filepath = "test.txt";
    ClassLoader classLoader = getClass().getClassLoader();
    file = new File(Objects.requireNonNull(classLoader.getResource(filepath)).getFile());
    fileParser = new FileParser();
  }

  @Test
  public void parse() {
    Map<String, List<Item>> parsedResult = fileParser.parse(file);
    log.info(parsedResult.entrySet().toString());
    TestCase.assertNotNull("Not null", parsedResult);
  }
}